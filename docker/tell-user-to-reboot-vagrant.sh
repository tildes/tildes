#!/bin/sh

echo "*******************************************************************"
echo
echo
echo "  If this is the first time provisioning the Docker container,"
echo "  PLEASE RESTART THE CONTAINER, so systemd services run properly!"
echo
echo "  Run:"
echo
echo "  $ vagrant halt && vagrant up --provider=docker"
echo
echo
echo "*******************************************************************"
