# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_CONFIG_VERSION = "2"

Vagrant.configure(VAGRANT_CONFIG_VERSION) do |config|
  config.vm.box = "debian/bookworm64"

  # Main application folder
  config.vm.synced_folder "tildes/", "/opt/tildes/"

  config.vm.synced_folder "ansible/", "/srv/ansible"

  config.vm.network "forwarded_port", guest: 443, host: 4443
  config.vm.network "forwarded_port", guest: 9090, host: 9090

  config.vm.provision "ansible_local" do |ansible|
    ansible.install = true
    ansible.install_mode = "pip"
    ansible.version = "10.6.0"
    ansible.pip_install_cmd = "sudo apt-get install -y python3-pip"
    ansible.pip_args = "--break-system-packages"

    # put the VM into the "dev" and "app_server" Ansible groups
    ansible.groups = {
        "dev" => ["default"],
        "app_server" => ["default"],
    }

    ansible.galaxy_role_file = "ansible/requirements.yml"
    ansible.playbook = "ansible/playbook.yml"
  end

  config.vm.provider "virtualbox" do |vb|
      vb.memory = "4096"
      vb.cpus = "4"
  end

  config.vm.provider "docker" do |d, override|
    # Docker does not require config.vm.box
    override.vm.box = nil
    # Instead, specify build_dir where Dockerfile is located.
    d.build_dir = "./docker"
    d.dockerfile = "Dockerfile-for-vagrant"

    # Keep Docker container running indefinitely
    d.remains_running = true
    d.create_args = ["--detach", "--tty"]

    # SSH configuration
    d.has_ssh = true

    # Tell user to restart Docker container after Ansible provisioning
    override.vm.provision "shell", path: "./docker/tell-user-to-reboot-vagrant.sh"
  end
end
